import { Component, OnInit } from '@angular/core';
import {MoviesAccountService} from '../../services/movies-account.service';

@Component({
  selector: 'app-favorites',
  templateUrl: './favorites.component.html',
  styleUrls: ['./favorites.component.css']
})
export class FavoritesComponent implements OnInit {

  favs_movies: any[] = [];
  favs_tv: any[]= [];

  constructor(
    private movies_account_srv: MoviesAccountService
  ) { }

  ngOnInit() {
    this.getFavsMovies();
    this.getFavsTv();
  }

  getFavsMovies() {
    this.movies_account_srv.getFavsMovies().subscribe(
      resp => {
        console.log(resp);
        this.favs_movies = resp.results;
      }
    );
  }

  getFavsTv() {
    this.movies_account_srv.getFavsTv().subscribe(
      resp => {
        console.log(resp);
        this.favs_tv = resp.results;
      }
    );
  }

}
