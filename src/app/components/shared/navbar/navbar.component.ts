import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {MovieAuthService} from "../../../services/movie-auth.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {

  // isAuthenticated = false;

  user_data = null;

  constructor(
    private router: Router,
    private movie_auth_srv: MovieAuthService
  ) { }

  ngOnInit() {
    this.movie_auth_srv.checkLoginStatus();
  }

  getUserData() {
  }

  /*checkSessionStatus() {
    this.movie_auth_srv.isAuthenticated().subscribe(
      resp => {
        this.isAuthenticated = !!(resp.username);
    },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log('Unauthorized');
          this.isAuthenticated = false;
        }
      }
    );
  }*/

  buscar(q: string) {
    if (q.length) {
      this.router.navigate(['/search', q]);
    }
  }

  login() {
    this.router.navigate(['/login']);
  }

  logout() {
    this.movie_auth_srv.deleteSession();
  }

}
