import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../../services/movies.service';

@Component({
  selector: 'app-tvshows',
  templateUrl: './tvshows.component.html',
  styleUrls: ['./tvshows.component.css']
})
export class TvshowsComponent implements OnInit {

  tv: any[] = [];
  results_page = 1;

  constructor(
    private movies_srv: MoviesService
  ) { }

  ngOnInit() {
    this.getShows();
  }

  getShows() {
    this.movies_srv.getTvTrends(this.results_page).subscribe(
      resp => {
        this.tv = this.tv.concat(resp.results);
        this.results_page++;
      }
    );
  }

}
