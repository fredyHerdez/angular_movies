import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../../services/movies.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  items: any[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private movies_srv: MoviesService
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(
      params => {
        this.getSearch(params['q']);
      }
    );
  }

  getSearch(q: string) {
    this.movies_srv.search(q).subscribe(
      resp => {
        this.items = resp.results;
        console.log(this.items);
      }
    );
  }

}
