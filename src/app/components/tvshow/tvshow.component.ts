import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../../services/movies.service';
import {ActivatedRoute} from '@angular/router';
import {MovieAuthService} from '../../services/movie-auth.service';
import {MoviesAccountService} from '../../services/movies-account.service';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-tvshow',
  templateUrl: './tvshow.component.html',
  styleUrls: ['./tvshow.component.css']
})
export class TvshowComponent implements OnInit {

  tv: any = [];
  recomendatios: any[] = [];
  acc_state: any = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private movie_srv: MoviesService,
    private movies_auth_srv: MovieAuthService,
    private movies_account_srv: MoviesAccountService
  ) {
  }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.getTv(params['id']);
    });
  }

  getTv(id: string) {
    this.movie_srv.getTv(id).subscribe(
      resp => {
        this.tv = resp;
        this.getRecommendations(id);
        // Verificar si esta autenticado y, si es así, entonces obtener el status del show

        this.movies_auth_srv.isAuthenticated().subscribe(
          user_info => {
            this.movies_auth_srv.isUserAuthenticated = !!(user_info.username);
            if ( this.movies_auth_srv.isUserAuthenticated) {
              this.getAccountState();
            }
          },
          (error: HttpErrorResponse) => {
            console.log('No estamos logueados');
          }
      );
      }
    );
  }

  getRecommendations(id: string) {
    this.movie_srv.getTvRecommendations(id).subscribe(
      resp => {
        this.recomendatios = resp.results.slice(0, 8);
      }
    );
  }

  getAccountState() {
    this.movies_account_srv.getAccountState('tv', this.tv.id).subscribe(resp => { this.acc_state = resp; console.log(resp); } );
  }

  setFavorite() {
    if ( this.movies_auth_srv.isUserAuthenticated) {
      this.movies_account_srv.markAsFav('tv', !(this.acc_state && this.acc_state.favorite), this.tv.id).subscribe(
        resp => {
          console.log(resp);
          this.getAccountState();
          console.log( !(this.acc_state && this.acc_state.favorite) ? 'Se agrego a favs' : ' Se quito de favs' );
        }
      );
    }
  }

  addToWatchlist() {
    if ( this.movies_auth_srv.isUserAuthenticated ){
      this.movies_account_srv.addToWatchList(
        'tv',
        !(this.acc_state && this.acc_state.watchlist ),
        this.tv.id ).subscribe(
          resp => {
            console.log(resp);
            this.getAccountState();
            console.log( !(this.acc_state && this.acc_state.watchlist) ? ' Se agrego al watchlist' : 'No se agrego al watchlist');
          },
      );
    }
  }

}
