import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../../services/movies.service';
import {ActivatedRoute} from "@angular/router";
import {MovieAuthService} from "../../services/movie-auth.service";
import {MoviesAccountService} from "../../services/movies-account.service";
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  movie: any = null;
  loaded = false;
  cast: any[] = [];
  crew: any[] = [];
  acc_state: any = null;

  constructor(
    private activatedRoute: ActivatedRoute,
    private movies_srv: MoviesService,
    private movies_auth_srv: MovieAuthService,
    private movies_account_srv: MoviesAccountService
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.getMovie(params['id']);
    });
  }

  getMovie(id: string) {
    this.movies_srv.getMovie(id).subscribe(
      resp => {
        this.movie = resp;
        this.loaded = true;
        this.getMovieCredits(id);

        // checar si el user esta logueado, entonces mandara  pedir los datos

        this.movies_auth_srv.isAuthenticated().subscribe(
          user_info => {
            this.movies_auth_srv.isUserAuthenticated = !!(user_info.username);
            if ( this.movies_auth_srv.isUserAuthenticated ) {
              this.getAccountState();
            }
          },
          (error: HttpErrorResponse) => {
            this.movies_auth_srv.isUserAuthenticated = false;
            console.log('No estamos logueados');
          }
        );
      }
    );
  }

  getMovieCredits(id: string) {
    this.movies_srv.getMovieCredits(id).subscribe(
      resp => {
        this.cast = resp.cast;
        this.cast = this.cast.slice(0, 6);
        this.crew = resp.crew;
      }
    );
  }

  getAccountState() {
    this.movies_account_srv.getAccountState('movie', this.movie.id).subscribe(resp => { this.acc_state = resp; console.log(resp); } );
  }

  setFavorite() {
    if (this.movies_auth_srv.isUserAuthenticated) {
      this.movies_account_srv.markAsFav('movie', !(this.acc_state && this.acc_state.favorite), this.movie.id).subscribe(
        resp => {
          console.log();
          this.getAccountState();
          console.log( !(this.acc_state && this.acc_state.favorite) ? 'Se agrego a favs' : ' Se quito de favs' );
        }
      );
    }
  }


}
