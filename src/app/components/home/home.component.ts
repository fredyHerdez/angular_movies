import { Component, OnInit } from '@angular/core';
import {MoviesService} from "../../services/movies.service";
import {ActivatedRoute} from "@angular/router";
import {MovieAuthService} from "../../services/movie-auth.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  movies: any[]= [];
  tv: any[]= [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private movies_srv: MoviesService,
    private auth_srv: MovieAuthService
  ) {

  }

  ngOnInit() {
    this.getMovies();
    this.getTv();
    /*this.activatedRoute.params.subscribe(params => {
      console.log(params);
    });*/

    console.log(localStorage.getItem('__SESSION__'));
    /*this.auth_srv.getRequestToken().subscribe(resp => {
      console.log(resp);
    });*/
  }

  getMovies() {
    this.movies_srv.getMovieTrends().subscribe(res => {
      this.movies = res.results;
      // console.log(this.movies);
    });
  }

  getTv() {
    this.movies_srv.getTvTrends().subscribe(resp => {
      this.tv = resp.results;
      //console.log(this.tv);
    });
  }

}
