import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthAllowedComponent } from './auth-allowed.component';

describe('AuthAllowedComponent', () => {
  let component: AuthAllowedComponent;
  let fixture: ComponentFixture<AuthAllowedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthAllowedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthAllowedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
