import { Component, OnInit } from '@angular/core';
import {MovieAuthService} from '../../services/movie-auth.service';
import {Router} from '@angular/router';
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-auth-allowed',
  templateUrl: './auth-allowed.component.html',
  styleUrls: ['./auth-allowed.component.css']
})
export class AuthAllowedComponent implements OnInit {

  constructor(
    private movie_auth_srv: MovieAuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.getAccessToken();
  }

  getAccessToken() {
    const rt = JSON.parse(localStorage.getItem('request_token'));

    if ( rt ) {
      localStorage.removeItem('request_token');

      // Crear sesion en el server de la api

      this.movie_auth_srv.createSession(rt.request_token).subscribe(
        resp => {
          console.log(resp);
          this.movie_auth_srv.startSession(resp);
          this.router.navigate(['/home']);
        },
        (error: HttpErrorResponse) => {
          console.log(error);
          if ( error.status === 401) {
            console.log('No se pudo loguear con el user');
          }

          this.router.navigate(['/home']);
        }
      );
    }else {
      console.log(' no hay request token almacenado en el navegador');
    }
  }

}
