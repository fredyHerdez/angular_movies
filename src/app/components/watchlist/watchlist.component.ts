import { Component, OnInit } from '@angular/core';
import {MoviesAccountService} from '../../services/movies-account.service';

@Component({
  selector: 'app-watchlist',
  templateUrl: './watchlist.component.html',
  styleUrls: ['./watchlist.component.css']
})
export class WatchlistComponent implements OnInit {

  tv_list: any[] = [];
  movie_list: any[] = [];

  constructor(
    private movies_account_srv: MoviesAccountService
  ) { }

  ngOnInit() {
    this.getMovieList();
    this.getTvList();
  }

  getTvList() {
    this.movies_account_srv.getTvWatchList().subscribe(
      resp => {
        this.tv_list = resp.results;
      }
    );
  }

  getMovieList() {
    this.movies_account_srv.getMoviesWatchList().subscribe(
      resp => {
        this.movie_list = resp.results;
      }
    );
  }

}
