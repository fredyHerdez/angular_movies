import { Component, OnInit } from '@angular/core';
import {MovieAuthService} from '../../services/movie-auth.service';
import {Router} from '@angular/router';
import {DomSanitizer, SafeUrl} from '@angular/platform-browser' ;
import {HttpErrorResponse} from "@angular/common/http";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  /*Desde este componente se adminsitra toda la autenticación.
  1.- Primero se verifica que no haya nadie logueado
  2.- Se manda a crear un request token a través del servicio.
  3.- Abrir un iframe que le diga al usuario que se loguee y acepte... una vez que redirija al link en especifico, entonces cerrar el iframe
  4.- Una vez cerrado el iframe, mandar a obtener el access token.
  5.- Se guarda la sesion  (userid, accesstoken y así)....*/

  // frameUrl: SafeUrl = '';

  constructor(
    private movie_auth_service: MovieAuthService,
    private router: Router,
    private sanitizer: DomSanitizer
  ) {
  }

  ngOnInit() {
    // this.frameUrl = this.sanitizer.bypassSecurityTrustResourceUrl('https://www.themoviedb.org/documentation/api/discover');
    this.checkLoginStatus();
  }

  checkLoginStatus() {
    this.movie_auth_service.isAuthenticated().subscribe(
      resp => {
        console.log('Se ha recibido buen resultado');
        console.log(resp);
        if (resp.username) {
          // Guardar la data en localStorage
          console.log('ya esta iniciada la sesión muy bien');
          console.log(resp);
          this.router.navigate(['/home']);
        }else {
          this.getRequestToken();
        }
    },
      (error: HttpErrorResponse) => {
        console.log(error);
        if (error.status === 401) {
          console.log('Unauthorized');
          this.getRequestToken();
        }
      }
    );
  }

  getRequestToken() {

    // esto es con la v4
    /*this.movie_auth_service.getRequestToken().subscribe(
      resp => {
        const rt = resp.request_token; // usar este request token para que el usuario se loguee desde la app y acepte todito
        // this.frameUrl = this.sanitizer.bypassSecurityTrustUrl(`https://www.themoviedb.org/auth/access?request_token=${ rt }`);

        // Guardar el request_token en local storage
        localStorage.setItem('request_token', rt);

        window.location.href = `https://www.themoviedb.org/auth/access?request_token=${ rt }`;
      }
    );*/


    // esto es con la v3

    this.movie_auth_service.getRequestToken().subscribe(
      resp => {
        // Guardar las cosas en el localStorage
        console.log(resp);

        if (resp.success) {
          const rt = {
            expires_in: resp.expires_in,
            request_token: resp.request_token,
          };

          localStorage.setItem('request_token', JSON.stringify(rt));
          window.location.href = `https://www.themoviedb.org/authenticate/${ rt.request_token }?redirect_to=http://localhost:4200/auth/allowed`;
        }else {
          // No se inició sesión en la API y retornar un error
          console.log('No se pudo obtener un request_token');
        }
      }
    );
  }

}
