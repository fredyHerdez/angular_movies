import { Component, OnInit } from '@angular/core';
import {MoviesService} from '../../services/movies.service';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {

  movies: any[] = [];
  results_page = 1;

  constructor(
    private movies_srv: MoviesService
  ) { }

  ngOnInit() {
    this.getMovies();
  }

  getMovies() {
    this.movies_srv.getMovieTrends(this.results_page).subscribe(
      resp => {
        console.log(resp);
        this.movies = this.movies.concat(resp.results);
        console.log(this.movies);
        this.results_page++;
      }
    );
  }

}
