import { BrowserModule } from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import { HttpModule, JsonpModule} from '@angular/http';


import { AppComponent } from './app.component';
import {MoviesService} from './services/movies.service';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { DetailsComponent } from './components/details/details.component';
import { SearchComponent } from './components/search/search.component';
import { MoviesComponent } from './components/movies/movies.component';
import { TvshowsComponent } from './components/tvshows/tvshows.component';
import { HomeComponent } from './components/home/home.component';
import { APP_ROUTING } from './app.routes';
import { MovieComponent } from './components/movie/movie.component';
import { TvshowComponent } from './components/tvshow/tvshow.component';
import {MovieAuthService} from './services/movie-auth.service';
import { LoginComponent } from './components/login/login.component';
import { AuthAllowedComponent } from './components/auth-allowed/auth-allowed.component';
import { FavoritesComponent } from './components/favorites/favorites.component';
import {MoviesAccountService} from './services/movies-account.service';
import {AuthGuardService} from './services/auth-guard.service';
import { WatchlistComponent } from './components/watchlist/watchlist.component';



@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DetailsComponent,
    SearchComponent,
    MoviesComponent,
    TvshowsComponent,
    HomeComponent,
    MovieComponent,
    TvshowComponent,
    LoginComponent,
    AuthAllowedComponent,
    FavoritesComponent,
    WatchlistComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    JsonpModule,
    APP_ROUTING
  ],
  providers: [ MoviesService, MovieAuthService, MoviesAccountService, AuthGuardService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
