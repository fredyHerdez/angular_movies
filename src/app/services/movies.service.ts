import { Injectable } from '@angular/core';

import { Http, Jsonp} from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class MoviesService {
  api_key= '944454b13bd1bf21898aa0b031319943';
  main_url= 'https://api.themoviedb.org/3';

  SORT_BY = {
    popularity_asc: 1,
    popularity_desc: 2,
    date_asc: 3,
    date_desc: 4
  };

  constructor(
    private jsonp: Jsonp,
    private http: Http
  ) {

  }


  // MOVIE

  getMovieTrends(
    page: number = 1
  ) {
    let url = `${ this.main_url }/discover/movie?sort_by=popularity.desc&api_key=${ this.api_key }&language=es-MX&page=${ page }`;
    return this.http.get(url).map( res => res.json());
  }

  searchMovie(query: string) {
  }

  getMovie(id: string) {
    // `${ this.main_url_v4 }/discover/tv?sort_by=popularity.desc&api_key=${ this.api_key }&language=es-MX`
    let url = `${ this.main_url }/movie/${ id }?api_key=${ this.api_key }&language=es-MX`;
    return this.http.get(url).map(res => res.json());
  }

  getMovieCredits(id: string) {
    let url = `${ this.main_url }/movie/${ id }/credits?api_key=${ this.api_key }&language=es-MX`;
    return this.http.get(url).map(res => res.json());
  }

  // TV

  getTv(id: string) {
    let url = `${ this.main_url }/tv/${ id }?api_key=${ this.api_key }&language=es-MX`;
    return this.http.get(url).map(res => res.json());
  }

  getTvTrends(
    page: number = 1
  ) {
    let url = `${ this.main_url }/discover/tv?sort_by=popularity.desc&api_key=${ this.api_key }&language=es-MX&page=${ page }`;
    return this.http.get(url).map( res => res.json());
  }

  // Recomendaciones de acuerdo a un show que se este visualizando
  getTvRecommendations(id: string) {
    let url = `${ this.main_url }/tv/${ id }/recommendations?api_key=${ this.api_key }&language=es-MX`;
    return this.http.get(url).map( res => res.json());
  }

  searchtTv(q: string) {
    let url = `${ this.main_url }/search/multi?api_key=${ this.api_key}&query=${ encodeURI(q) }`;
    return this.http.get(url).map(res => res.json());
  }

  // ALL

  search(q: string) {
    let url = `${ this.main_url }/search/multi?api_key=${ this.api_key }&language=es-MX&query=${ encodeURI(q) }`;
    return this.http.get(url).map(res => res.json());
  }


}
