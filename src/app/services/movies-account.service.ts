import { Injectable } from '@angular/core';
import {Http, Jsonp, RequestOptions, Headers} from '@angular/http';
import {MovieAuthService} from './movie-auth.service';

@Injectable()
export class MoviesAccountService {

  api_key= '944454b13bd1bf21898aa0b031319943';
  main_url= 'https://api.themoviedb.org/3';
  public session_id = null;
  public user_data = null;

  constructor(
    private jsonp: Jsonp,
    private http: Http,
    private  movies_auth_srv: MovieAuthService
  ) {
    this.movies_auth_srv.checkLoginStatus();
    this.user_data = this.movies_auth_srv.getLocalUserData();
  }


  //

  private buildFinalUrlStr(url: string, params: any[] = []) {
    return [`${ url }?language=es-MX&api_key=${ this.api_key }&session_id=${ this.movies_auth_srv.getLocalSession().session_id }`, params.join('&')].join('&');
  }

  getFavsMovies() {
    return this.http.get(this.buildFinalUrlStr(`${ this.main_url }/account/${ this.user_data.id }/favorite/movies`)).map(resp => resp.json());
  }

  getFavsTv() {
    return this.http.get(this.buildFinalUrlStr(`${ this.main_url }/account/${ this.user_data.id }/favorite/tv`)).map(resp => resp.json());
  }

  getMovieStatus( movie_id: string) {
    // return this.http.get(this.buildFinalUrlStr(`${ this.main_url }/movie/${ movie_id }/account_states`)).map(resp => resp.json());
  }

  getAccountState(type: string, id: string) {
    return this.http.get(this.buildFinalUrlStr(`${ this.main_url }/${ type }/${ id }/account_states`)).map(resp => resp.json());
  }

  getTvWatchList() {
    return this.http.get(this.buildFinalUrlStr(`${ this.main_url }/account/${ this.user_data.id }/watchlist/tv`)).map(resp => resp.json());
  }

  getMoviesWatchList() {
    return this.http.get(this.buildFinalUrlStr(`${ this.main_url }/account/${ this.user_data.id }/watchlist/movies`)).map(resp => resp.json());
  }


  // POST

  markAsFav(type: string, value: boolean, id: string) {
    const body: any = {
      'media_type': type,
      'media_id': id,
      'favorite': value
    };

    const header = new Headers();
    header.append('Content-Type', 'application/json;charset=utf-8');
    const request_options = new RequestOptions({ headers: header });

    return this.http.post(
      this.buildFinalUrlStr(
        `${ this.main_url }/account/${ this.user_data.id }/favorite`),
        body,
        request_options
    ).map(resp => resp.json());
  }

  addToWatchList( type: string, value: boolean, id: string) {
    const body = {
      'media_type': type,
      'media_id': id,
      'watchlist': value
    };

    const header = new Headers();
    header.append('Content-Type', 'application/json;charset=utf-8');
    const request_options = new RequestOptions({ headers: header });

    return this.http.post(
      this.buildFinalUrlStr(
        `${ this.main_url }/account/${ this.user_data.id }/watchlist`),
      body,
      request_options
    ).map(resp => resp.json());
  }


}
