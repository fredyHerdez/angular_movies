import { TestBed, inject } from '@angular/core/testing';

import { MoviesAccountService } from './movies-account.service';

describe('MoviesAccountService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MoviesAccountService]
    });
  });

  it('should be created', inject([MoviesAccountService], (service: MoviesAccountService) => {
    expect(service).toBeTruthy();
  }));
});
