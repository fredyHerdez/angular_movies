import { Injectable } from '@angular/core';
import {Http, Jsonp, RequestOptions, Headers} from '@angular/http';
import {HttpErrorResponse} from "@angular/common/http";

@Injectable()
export class MovieAuthService {

  /*
  Este servicio consumirá la API v4 de TMDB, en est aPI esta mejorada la autenticación.... Así que cambiará el url y todo
  */

  main_url_v4 = 'https://api.themoviedb.org/4/auth';
  main_url_v3 = 'https://api.themoviedb.org/3/authentication';
  access_token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI5NDQ0NTRiMTNiZDFiZjIxODk4YWEwYjAzMTMxOTk0MyIsInN1YiI6IjVhMWMyYWNkYzNhMzY4MGI4YTA1NDVkNiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.igyk2LBG0Oc6HAhe6w6TXGGffIlxt3vVO9SfcLhDLj0';
  api_key = '944454b13bd1bf21898aa0b031319943';
  isUserAuthenticated = false;
  counter = 1;
  user_data = null;

  constructor(
    private jsonp: Jsonp,
    private http: Http
  ) {
  }

  // Función global para detectar que este logueado
  checkLoginStatus() {
    this.isAuthenticated().subscribe(
      resp => {
        this.isUserAuthenticated = !!(resp.username);
        this.user_data = resp;
        localStorage.setItem('__USER_DATA__', JSON.stringify(this.user_data));
      },
      (error: HttpErrorResponse) => {
        if (error.status === 401) {
          console.log('Unauthorized');
          this.isUserAuthenticated = false;
        }
      }
    );
  }


  // Obtener un request_token para después ponerlo en el form de tmdb
  getRequestToken() {
    // Verificar si no hay un access_token y un user_id almacenados en local storage y, si hay, no hacer esto...

    // Esto comentado se hace con la API v4, la cual no tiene ciertas funciones...
    /*const header = new Headers();
    header.append('Content-Type', 'application/json;charset=utf-8');
    header.append('Authorization', `Bearer ${ this.access_token }`);
    const request_options = new RequestOptions({ headers: header });

    return this.http.post(
      `${ this.main_url_v4 }/request_token`,
      {'redirect_to': 'http://localhost:4200/auth/allowed'},
      request_options
    ).map(
      resp => resp.json()
    );*/

    // API v3

    return this.http.get(`${ this.main_url_v3}/token/new?api_key=${ this.api_key }`).map(resp => resp.json());

  }

  getAccessToken(request_token: string) {
    const header = new Headers();
    header.append('Content-Type', 'application/json;charset=utf-8');
    header.append('Authorization', `Bearer ${ this.access_token }`);
    const request_options = new RequestOptions({ headers: header });


    return this.http.post(
      `${ this.main_url_v4 }/access_token`,
      {'request_token': request_token },
      request_options).map(
      resp =>  resp.json()
    );
  }



  //v3

  // Iniciar sesion en la API de Tmdb
  createSession(request_token: string) {
    return this.http.get(
      `https://api.themoviedb.org/3/authentication/session/new?api_key=${ this.api_key }&request_token=${ request_token }`
    ).map(
      resp => resp.json()
    );
  }

  // ---- ESCRIBIR LAS SESIONES Y LOS DATOS EN LOCAL STORAGE O ALGO POR EL ESTILO----

  // Iniciar la sesión, recibiendo como parámetro el JSON que se recibió de la API
  startSession(resp) {

    // API v4
    /*if ( resp.success) {
      const session = {
        account_id: resp.account_id,
        access_token: resp.access_token
      };
      localStorage.setItem('_SESSION_', JSON.stringify(session));
    }*/

    // API v3

    if (resp.success) {
      const session = {
        session_id: resp.session_id
      };
      localStorage.setItem('__SESSION__', JSON.stringify(session));

      this.checkLoginStatus();
    } else {
      console.log('No se pudo iniciar la sesion en la api');
    }
  }

  deleteSession() {
    localStorage.removeItem('__SESSION__');
    this.isUserAuthenticated = false;
  }

  isAuthenticated() {
    const session = JSON.parse(localStorage.getItem('__SESSION__'));
    const session_id = (session) ? session.session_id : '';
    return this.http.get(
      `https://api.themoviedb.org/3/account?api_key=${ this.api_key }&session_id=${ session_id }`
    ).map(
      resp => resp.json()
    );
  }

  getLocalSession() {
    return JSON.parse(localStorage.getItem('__SESSION__'));
  }

  getLocalUserData() {
    return JSON.parse(localStorage.getItem('__USER_DATA__'));
  }

}
