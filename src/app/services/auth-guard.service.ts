import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';
import {MovieAuthService} from './movie-auth.service';

@Injectable()
export class AuthGuardService implements  CanActivate {

  constructor( private movie_auth_srv: MovieAuthService ) { }

  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return this.movie_auth_srv.isUserAuthenticated;
  }
}
