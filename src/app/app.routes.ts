
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {MoviesComponent} from './components/movies/movies.component';
import {TvshowsComponent} from './components/tvshows/tvshows.component';
import {SearchComponent} from './components/search/search.component';
import {MovieComponent} from './components/movie/movie.component';
import {TvshowComponent} from './components/tvshow/tvshow.component';
import {LoginComponent} from './components/login/login.component';
import {AuthAllowedComponent} from './components/auth-allowed/auth-allowed.component';
import {FavoritesComponent} from './components/favorites/favorites.component';
import {AuthGuardService} from './services/auth-guard.service';
import {WatchlistComponent} from './components/watchlist/watchlist.component';

const APP_ROUTES: Routes = [
  { path: 'home', component: HomeComponent},
  { path: 'movies/:id', component: MovieComponent},
  { path: 'movies', component: MoviesComponent},
  { path: 'tv/:id', component: TvshowComponent},
  { path: 'tv', component: TvshowsComponent},
  { path: 'search/:q', component: SearchComponent},
  { path: 'login', component: LoginComponent},
  { path: 'auth/allowed', component: AuthAllowedComponent},
  { path: 'favorites', component: FavoritesComponent, canActivate: [ AuthGuardService ] },
  { path: 'watchlist', component: WatchlistComponent, canActivate: [ AuthGuardService ] },
  { path: '**', pathMatch: 'full', redirectTo: 'home'}
];

export const APP_ROUTING = RouterModule.forRoot(APP_ROUTES);
